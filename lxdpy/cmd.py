
import sys
import os.path
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from pylxd import Client

def client_auth(lxd_endpoint):
    path = os.path.abspath(os.path.dirname(__file__))
    lxd_crt = ('~/lxd.crt')
    lxd_key = ('~/lxd.key')

    lxd_endpoint = 'https://' + lxd_endpoint + ':8443'

    client = Client(
        endpoint=lxd_endpoint,
        cert=(lxd_crt, lxd_key),
        verify=False
    )

    return(client)


def get_containers(lxd_endpoint):
   client = client_auth(lxd_endpoint)

   return client.containers.all()


def get_container(c_name, lxd_endpoint):
    client = client_auth(lxd_endpoint)

    return client.containers.get(c_name)
