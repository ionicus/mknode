
from pyghmi.ipmi import command

def exec_ipmi_cmd(bmc_creds):
    bmc_ip, bmc_user, bmc_pass = bmc_creds

    ipmi_cmd = command.Command(
        bmc=bmc_ip, userid=bmc_user, password=bmc_pass, port='623'
    )

    return(ipmi_cmd)


def get_power(bmc_creds):
    ipmi_cmd = exec_ipmi_cmd(bmc_creds)

    power = ipmi_cmd.get_power()

    return(power['powerstate'])


def get_inventory(bmc_creds):
    ipmi_cmd = exec_ipmi_cmd(bmc_creds)

    inventory = ipmi_cmd.get_inventory()

    return(inventory)


def test():
    assert as_int('1') == 1


if __name__ == '__main__':
    test()
