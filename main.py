#!/usr/bin/env python3

import sys
import os.path
import json
import bmcpy.cmd
import lxdpy.cmd

manifest_file = 'manifest.json'

def get_manifest(manifest_file):
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, manifest_file)

    with open(filepath) as file:
        return(json.load(file))


def main():
    # load json manifest of all machines
    # { mach_name: [ bmc_ip, bmc_user, bmc_pass, lxd_ip ], }
    manifest = get_manifest(manifest_file)
    
    # Get powerstate for all bmcs in manifest
    for mach_name in manifest:
        bmc_creds = manifest[mach_name][:3]
        lxd_endpoint = manifest[mach_name][3]

        power_state = bmcpy.cmd.get_power(bmc_creds)
        inventory = bmcpy.cmd.get_inventory(bmc_creds)

        print(mach_name + ':\t' + power_state)

        # If power is 'on' query the lxd api
        if power_state == 'on' and bool(lxd_endpoint):
            containers = lxdpy.cmd.get_containers(lxd_endpoint)

            for c in containers:
                print('\t'.join([c.name,c.status]))

                if c.status == 'Running':
                    eth0_net = c.state().network['eth0']['addresses']
                    for en in eth0_net:
                        print('\t\t' + en['address'])

    print('\nDone!\n')


if __name__ == '__main__':
    main()
